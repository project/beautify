# Beautify

The Beautify module buffers the HTML output of Drupal and processes it through a formatter - aka beautifier plugin.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/beautify).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/beautify).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Download the module
   `composer require drupal/beautify`
2. Enable the module
   `drush en beautify`
3. Go to admin page Admin > Config > Development > Beautifiers
4. Select the default beautifier and edit the configuration
5. (optional) Run `drush config-export` to deploy your changes


## Maintainers

- Matthieu Scarset - [matthieuscarset](https://www.drupal.org/u/matthieuscarset)
- Richard Burford - [psynaptic](https://www.drupal.org/u/psynaptic)
- Balazs Dianiska - [snufkin](https://www.drupal.org/u/snufkin)
- Mike Carper - [mikeytown2](https://www.drupal.org/u/mikeytown2)
