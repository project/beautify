<?php

namespace Drupal\beautify\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Drupal\Core\Plugin\PluginWithFormsInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure our custom settings form.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'beautify.settings';

  /**
   * The beautifier plugin manager.
   *
   * @var \Drupal\Component\Plugin\PluginManagerInterface
   */
  protected $beautifierManager;

  /**
   * The plugin form manager.
   *
   * @var \Drupal\Core\Plugin\PluginFormFactoryInterface
   */
  protected $pluginFormFactory;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->beautifierManager = $container->get('plugin.manager.beautifier');
    $instance->pluginFormFactory = $container->get('plugin_form.factory');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'beautify_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    $config_names = [];
    $config_names[] = static::SETTINGS;
    foreach (array_keys($this->beautifierManager->getDefinitions()) as $plugin_id) {
      $config_names[] = 'beautify.beautifier.' . $plugin_id . '.settings';
    }
    return $config_names;
  }

  /**
   * Get a plugin form.
   */
  public function getPluginForm($plugin, $operation) {
    if ($plugin instanceof PluginWithFormsInterface) {
      if ($plugin->hasFormClass($operation)) {
        return $this->pluginFormFactory->createInstance($plugin, $operation);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);
    $default_plugin_id = $config->get('default_html_beautifier') ?? 'html';
    $beautifiers = $this->beautifierManager->getDefinitions();
    $plugins = array_column($beautifiers, 'label', 'id');
    uksort(
          $plugins, function ($a, $b) use ($default_plugin_id) {
              $a_weight = $a == $default_plugin_id ? 0 : 1;
              $b_weight = $b == $default_plugin_id ? 0 : 1;

              return $a_weight <=> $b_weight;
          }
      );

    $form['default_html_beautifier'] = [
      '#type' => 'select',
      '#title' => $this->t('Default HTML Beautifier'),
      '#options' => $plugins,
      '#empty_option' => $this->t('- Select -'),
      '#default_value' => $default_plugin_id,
    ];

    $form['admin_routes'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable on admin routes'),
      '#description' => $this->t('Checked if administrative routes should be beautified too.'),
      '#default_value' => $config->get('admin_routes'),
    ];

    $form['beautifiers'] = [
      '#type' => 'vertical_tabs',
      '#title' => $this->t('Beautifiers'),
    ];

    // Build plugin form.
    foreach ($plugins as $plugin_id => $plugin_title) {
      $form[$plugin_id . '_wrapper'] = [
        '#type' => 'details',
        '#title' => $plugin_title,
        '#group' => 'beautifiers',
      ];

      $form['#parents'] = [];
      $form['#tree'] = TRUE;

      $form[$plugin_id] = ['#parents' => []];
      $plugin_settings = [];
      if ($plugin_config = $this->config('beautify.beautifier.' . $plugin_id . '.settings')) {
        $plugin_settings = $plugin_config->getRawData();
      }
      $beautifier_plugin = $this->beautifierManager->createInstance($plugin_id, $plugin_settings);
      if ($plugin_form = $this->getPluginForm($beautifier_plugin, 'edit')) {
        $subform_state = SubformState::createForSubform($form[$plugin_id], $form, $form_state);
        $form[$plugin_id . '_wrapper'][$plugin_id] = $plugin_form->buildConfigurationForm($form[$plugin_id], $subform_state);
      }
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Save beautifier configurations.
    foreach (array_keys($this->beautifierManager->getDefinitions()) as $plugin_id) {
      $plugin_settings = $form_state->getValues()[$plugin_id] ?? [];
      $beautifier_plugin = $this->beautifierManager->createInstance($plugin_id, $plugin_settings);
      if ($plugin_form = $this->getPluginForm($beautifier_plugin, 'edit')) {
        $subform_state = SubformState::createForSubform($form[$plugin_id], $form, $form_state);
        $plugin_form->validateConfigurationForm($form[$plugin_id], $subform_state);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->configFactory->getEditable(static::SETTINGS)
      ->set('default_html_beautifier', (string) $form_state->getValue('default_html_beautifier'))
      ->set('admin_routes', $form_state->getValue('admin_routes'))
      ->save();

    // Save beautifier configurations.
    foreach (array_keys($this->beautifierManager->getDefinitions()) as $plugin_id) {
      $plugin_settings = $form_state->getValues()[$plugin_id] ?? [];
      $beautifier_plugin = $this->beautifierManager->createInstance($plugin_id, $plugin_settings);
      if ($plugin_form = $this->getPluginForm($beautifier_plugin, 'edit')) {
        $subform_state = SubformState::createForSubform($form[$plugin_id], $form, $form_state);
        $plugin_form->submitConfigurationForm($form[$plugin_id], $subform_state);
      }

      $plugin_config = $this->configFactory->getEditable('beautify.beautifier.' . $plugin_id . '.settings');
      foreach ($plugin_settings as $key => $value) {
        $plugin_config->set($key, $value);
      }
      $plugin_config->save();
    }

    parent::submitForm($form, $form_state);
  }

}
