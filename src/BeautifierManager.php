<?php

namespace Drupal\beautify;

use Drupal\Component\Plugin\FallbackPluginManagerInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Manages beautifier plugins.
 *
 * @see \Drupal\beautify\BeautifierPluginInterface
 * @see \Drupal\beautify\Plugin\Beautifier\BeautifierPluginBase
 * @see \Drupal\beautify\Annotation\Beautifier
 * @see hook_beautifier_info_alter()
 */
class BeautifierManager extends DefaultPluginManager implements BeautifierManagerInterface, FallbackPluginManagerInterface {

  /**
   * Constructs a new BeautifierManager.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler for the alter hook.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/Beautifier', $namespaces, $module_handler, 'Drupal\beautify\BeautifierPluginInterface', 'Drupal\beautify\Annotation\Beautifier');

    $this->alterInfo('beautifier_info');
    $this->setCacheBackend($cache_backend, 'beautifier_plugins');
  }

  /**
   * {@inheritdoc}
   */
  public function getFallbackPluginId($plugin_id, array $configuration = []) {
    return 'html';
  }

}
