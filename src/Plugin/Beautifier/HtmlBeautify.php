<?php

namespace Drupal\beautify\Plugin\Beautifier;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Wongyip\HTML\Beautify;

/**
 * @Beautifier(
 *   id = "html_beautify",
 *   label = @Translation("HTML Beautify"),
 *   defaults = {
 *     "indent_inner_html" = FALSE,
 *     "indent_char" = " ",
 *     "indent_size" = "4",
 *     "wrap_line_length" = "32768",
 *     "preserve_newlines" = FALSE,
 *     "preserve_newlines_max" = "32768",
 *     "indent_scripts" = "normal",
 *     "remove_comments" = TRUE,
 *     "compact" = FALSE
 *   }
 * )
 */
class HtmlBeautify extends BeautifierPluginBase {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $plugin_id = $this->getPluginId();

    $form[$plugin_id]['indent_inner_html']['#type'] = 'checkbox';
    $form[$plugin_id]['indent_char']['#type'] = 'hidden';
    $form[$plugin_id]['indent_size']['#type'] = 'number';
    $form[$plugin_id]['indent_size']['#min'] = 2;
    $form[$plugin_id]['indent_size']['#max'] = 12;

    $form[$plugin_id]['wrap_line_length']['#type'] = 'number';
    $form[$plugin_id]['wrap_line_length']['#placeholder'] = 32768;

    // @todo Implement array type.
    // $form[$plugin_id]['unformatted']['#type'] = ['code', 'pre'];

    $form[$plugin_id]['preserve_newlines']['#type'] = 'checkbox';
    $form[$plugin_id]['preserve_newlines_max']['#type'] = 'number';
    $form[$plugin_id]['preserve_newlines_max']['#placeholder'] = 32768;

    $form[$plugin_id]['indent_scripts']['#type'] = 'select';
    $form[$plugin_id]['indent_scripts']['#empty_option'] = $this->t('- Select -');
    $form[$plugin_id]['indent_scripts']['#options']['keep'] = $this->t('Keep');
    $form[$plugin_id]['indent_scripts']['#options']['separate'] = $this->t('Separate');
    $form[$plugin_id]['indent_scripts']['#options']['normal'] = $this->t('Normal');

    $form[$plugin_id]['help'] = [
      '#prefix' => $this->t('Full documentation') . '&nbsp;&raquo;&nbsp;',
      '#type' => 'link',
      '#title' => $this->t('HTML Beautify'),
      '#url' => Url::fromUri('https://github.com/wongyip/html-beautify'),
      '#attributes' => ['target' => '_blank'],
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function format($input) {
    $output = $this->input = $input;

    // Run beautifier.
    $output = Beautify::init($this->configuration)->beautify($output);

    // Remove comments (e.g. Twig debug).
    if ($this->configuration['remove_comments'] ?? FALSE) {
      $output = preg_replace('/<!--(.|\s)*?-->/', '', $output);
    }

    // Remove top/bottom empty lines.
    $output = trim($output);

    $this->output = $output;

    return $this->getOutput();
  }

}
