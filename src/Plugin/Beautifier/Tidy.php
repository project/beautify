<?php

namespace Drupal\beautify\Plugin\Beautifier;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * @Beautifier(
 *   id = "tidy",
 *   label = @Translation("Tidy"),
 *   defaults = {
 *     "encoding" = "utf8",
 *     "indent" = TRUE,
 *     "indent-spaces" = "2",
 *     "hide-comments" = TRUE,
 *     "drop-empty-elements" = FALSE,
 *     "drop-empty-paras" = FALSE,
 *     "merge-divs" = FALSE,
 *     "merge-spans" = FALSE,
 *     "quote-ampersand" = FALSE,
 *     "quote-nbsp" = FALSE,
 *     "coerce-endtags" = FALSE,
 *     "custom-tags" = "drupal-big-pipe-scripts-bottom-marker",
 *     "escape-scripts" = FALSE,
 *     "skip-nested" = FALSE,
 *     "wrap" = "1000",
 *     "compact" = FALSE
 *   }
 * )
 *
 * @see https://www.php.net/manual/en/tidy.construct.php
 * @see http://api.html-tidy.org/#quick-reference
 */
class Tidy extends BeautifierPluginBase {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $plugin_id = $this->getPluginId();

    // Disable "remove_comment" because same as "hide_comments" in Tidy.
    $form[$plugin_id]['remove_comments'] = ['#access' => FALSE];

    $number_options = ['indent-spaces', 'wrap'];
    foreach ($number_options as $key) {
      $form[$plugin_id][$key]['#type'] = 'number';
    }

    $boolean_options = [
      'indent',
      'hide-comments',
      'drop-empty-elements',
      'drop-empty-paras',
      'merge-divs',
      'merge-spans',
      'quote-ampersand',
      'quote-nbsp',
      'coerce-endtags',
      'escape-scripts',
      'skip-nested',
    ];
    foreach ($boolean_options as $key) {
      $form[$plugin_id][$key]['#type'] = 'checkbox';
    }

    $form[$plugin_id]['help'] = [
      '#prefix' => $this->t('Full documentation') . '&nbsp;&raquo;&nbsp;',
      '#type' => 'link',
      '#title' => $this->t('HTML Tidy 5 Reference'),
      '#url' => Url::fromUri('https://api.html-tidy.org/tidy/quickref_next.html'),
      '#attributes' => ['target' => '_blank'],
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function format($input) {
    // Skip now if Tidy not found.
    // @see https://www.php.net/manual/fr/tidy.setup.php.
    if (!class_exists('tidy')) {
      $this->logger->error($this->t('Tidy class not found'));
      return $input;
    }

    $this->input = $input;

    $encoding = $this->configuration['encoding'] ?? 'utf8';
    unset($this->configuration['encoding']);

    $indent_space = (int) ($this->configuration['indent-spaces'] ?? 2);
    $this->configuration['indent-spaces'] = $indent_space;

    unset($this->configuration['_core']);
    unset($this->configuration['compact']);
    unset($this->configuration['remove_comments']);

    $tidy = new \tidy();
    $tidy->parseString($input, $this->configuration, $encoding);
    $tidy->cleanRepair();
    $this->output = tidy_get_output($tidy);

    return $this->getOutput();
  }

}
