<?php

namespace Drupal\beautify\Plugin\Beautifier;

use Drupal\beautify\BeautifierPluginInterface;
use Drupal\beautify\PluginForm\BeautifierPluginForm;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Plugin\PluginWithFormsInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base beautifier plugin.
 */
class BeautifierPluginBase extends PluginBase implements BeautifierPluginInterface, PluginWithFormsInterface, ContainerFactoryPluginInterface {

  /**
   * A config object for this plugin.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * @var mixed|null
   */
  protected $input = NULL;

  /**
   * @var mixed|null
   */
  protected $output = NULL;

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * {@inheritDoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $config_factory, LoggerInterface $logger) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $settings = $config_factory->get("beautify.beautifier.$plugin_id.settings");
    $settings = $settings ? $settings->getRawData() : [];

    // Merge existing settings, defaults from Annotation and current configuration.
    $this->configuration += $settings + $this->getPluginDefinition()['defaults'];

    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
          $configuration,
          $plugin_id,
          $plugin_definition,
          $container->get('config.factory'),
          $container->get('logger.channel.beautify'),
      );
  }

  /**
   * {@inheritDoc}
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * {@inheritDoc}
   */
  public function getFormClass($operation) {
    return $this->pluginDefinition['forms'][$operation] ?? BeautifierPluginForm::class;
  }

  /**
   * {@inheritDoc}
   */
  public function hasFormClass($operation) {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $plugin_id = $this->getPluginId();

    $form['#tree'] = TRUE;

    foreach ($this->getConfiguration() as $key => $value) {
      if (strpos($key, '_') === 0) {
        continue;
      }

      $form[$plugin_id][$key] = [
        '#type' => 'textfield',
        '#title' => ucfirst($key),
        '#default_value' => $value,
      ];
    }

    // Common.
    $form[$plugin_id]['remove_comments'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Remove comments'),
      '#default_value' => $this->configuration['remove_comments'] ?? 0,
    ];

    $form[$plugin_id]['compact'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Compact (minify)'),
      '#default_value' => $this->configuration['compact'] ?? 0,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    // Nothing specific to validate.
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $plugin_id = $this->getPluginId();

    foreach ($form_state->getValues()[$plugin_id] ?? [] as $key => $value) {
      $this->configuration[$key] = Xss::filter($value);
    }
  }

  /**
   * {@inheritDoc}
   */
  public function format(mixed $content) {
    // Nothing happens by default.
    return $content;
  }

  /**
   * {@inheritDoc}
   */
  public function getInput() {
    return $this->input;
  }

  /**
   * {@inheritDoc}
   */
  public function getOutput() {
    if ($this->configuration['compact'] ?? FALSE) {
      $this->compact();
    }

    return $this->output;
  }

  /**
   * Minify HTML.
   */
  public function compact() {
    $output = $this->output;

    // Doctype.
    preg_match('/(.*)>/Umsi', $output, $matches);
    if ($doctype = $matches[0] ?? FALSE) {
      $output = str_replace($matches[0], '', $output);
      // Newline and 1 or more spaces.
      $doctype = preg_replace("@\n\s+@", " ", $doctype);
    }

    // Newline and 1 or more spaces.
    $output = preg_replace("@\n\s+@", " ", $output);
    // Three or more spaces.
    $output = preg_replace("@\s\s\s+@", " ", $output);
    // Closing tag marker and a new line.
    $output = preg_replace("@>\n@", "> ", $output);

    if ($doctype) {
      $output = $doctype . $output;
    }

    $this->output = $output;
  }

}
