<?php

namespace Drupal\beautify\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Beautifier annotation object.
 *
 * @Annotation
 *
 * @see \Drupal\beautifier\BeautifierManager
 *
 * @ingroup beautifier
 */
class Beautifier extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the beautifier type.
   *
   * @ingroup plugin_translatable
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $label;

  /**
   * An array of default settings.
   *
   * @var array
   */
  public $defaults = [];

  /**
   * An array of plugin forms per operation.
   *
   * @var array
   */
  public $forms = [];

}
