<?php

namespace Drupal\beautify;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Provides an interface for a plugin to format content.
 *
 * Plugins of this type need to be annotated with
 * \Drupal\beautify\Annotation\Beautifier annotation, and placed in the
 * Plugin\Beautifier namespace directory. They are managed by the
 * \Drupal\beautify\BeautifierManager plugin manager class. There is a base
 * class that may be helpful:
 * \Drupal\beautify\Plugin\Beautifier\BeautifierPluginBase.
 */
interface BeautifierPluginInterface extends PluginInspectionInterface {

  /**
   * Run the beautifier.
   *
   * @param mixed $content
   *   The content to beautify.
   */
  public function format(mixed $content);

  /**
   * Returns the original content to be formatted.
   *
   * @return mixed
   *   The source content, untouched.
   */
  public function getInput();

  /**
   * Returns the formatted content.
   *
   * @return mixed
   *   The input after passing through "format".
   */
  public function getOutput();

}
