<?php

namespace Drupal\beautify\EventSubscriber;

use Drupal\beautify\BeautifierManagerInterface;
use Drupal\beautify\BeautifierPluginInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Routing\AdminContext;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Respond to Kernel events to beautify HTML.
 *
 * @internal
 */
class BeautifyResponseFilter implements EventSubscriberInterface {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * The plugin manager.
   *
   * @var \Drupal\beautify\BeautifierManagerInterface
   */
  protected $manager;

  /**
   * The route admin context to determine whether a route is an admin one.
   *
   * @var \Drupal\Core\Routing\AdminContext
   */
  protected $adminContext;

  /**
   * Constructs the RequestCloseSubscriber object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   A config factory for retrieving required config objects.
   *   The module handler.
   * @param \Drupal\beautify\BeautifierManagerInterface $manager
   *   The beautifier manager.
   * @param \Drupal\Core\Routing\AdminContext $admin_context
   *   The route admin context service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, BeautifierManagerInterface $manager, AdminContext $admin_context) {
    $this->config = $config_factory;
    $this->manager = $manager;
    $this->adminContext = $admin_context;
  }

  /**
   * Act on recently generated HTML files.
   *
   * @param \Symfony\Component\HttpKernel\Event\ResponseEvent $event
   *   The response event.
   */
  public function onResponse(ResponseEvent $event) {
    // Skip admin route.
    $beautify_admin = $this->config->get('beautify.settings')->get('admin_routes') ?: FALSE;
    if ($this->adminContext->isAdminRoute() && !$beautify_admin) {
      return;
    }

    $response = $event->getResponse();
    $content_type = $response->headers->get('Content-Type');
    if (empty($content_type) || stripos((string) $content_type, 'text/html') === FALSE) {
      return;
    }

    $plugin_id = $this->config->get('beautify.settings')->get('default_html_beautifier');
    if (!$plugin_id) {
      return;
    }

    $plugin_options = $this->config->get("beautify.beautifier.$plugin_id.settings")->getOriginal();
    $plugin = $this->manager->createInstance($plugin_id, $plugin_options);
    if (!$plugin instanceof BeautifierPluginInterface) {
      return;
    }

    $html = $response->getContent();
    $clean = $plugin->format($html);

    $response->setContent($clean);
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [];

    // Should run after latest response filter (e.g. HtmlResponseBigPipeSubscriber).
    $last_core_response_filter_weight = -10000;
    $events[KernelEvents::RESPONSE][] = ['onResponse', $last_core_response_filter_weight - 1];

    return $events;
  }

}
